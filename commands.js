const TRANSLATION_TABLE ={
	N: { x: 0, y: 1 }, S: { x: 0, y: -1 },
	E: { x: 1, y: 0 }, W: { x: -1, y: 0 }
}
const ROTATION_TABLE = {
	R: { N: "E", S: "W", W: "N", E: "S" },
	L: { N: "W", S: "E", W: "S", E: "N" }
}

function translate(cmd, robot, grid) {
	let newx = robot.x + TRANSLATION_TABLE[robot.orientation].x
	let newy = robot.y + TRANSLATION_TABLE[robot.orientation].y

	if(newx > grid.x || newx < 0 || newy > grid.y || newy < 0) {
		const scent = String(robot.x) + ',' + String(robot.y)
		if(grid.scents.includes(scent)) return
		robot.lost = 'LOST'
		grid.scents.push(scent)
	}
	robot.x = newx
	robot.y = newy
}

function rotate(cmd, robot, grid) {
	robot.orientation = ROTATION_TABLE[cmd][robot.orientation]
}

module.exports = { 'F': translate, 'R': rotate, 'L': rotate }

# Martian Robots

## How to use
In order to test the sample input given in the [assignment](https://github.com/guidesmiths/interview-code-challenges/blob/master/node/martian-robots/instructions.md), just run `yarn start`. You should obtain the sample output in the same assignment README.

If you want to run all available tests (12), run `yarn test`.

If you want to test your own file, run `node robot.js <your file>`.

## Technological rationale

### Why a language processor ?
- Language processors are fast and give good error context and information
- Once generated the parser (in dev) you no longer need the language processor

## Extensibility
As required by the problem approach, the solution can be extended in several
ways.

### Add new command
One way could be the adition of a command, like a backwards (`B`) or a fast
forward movement. Two modifications should be required:

1. Add the new command and command name in `commands.js`: a function with the
signature `function <function name>(cmd, robot, grid)` should be implemented.
This function will receive the command char (i.e. `B`), the robot structure 
(`x`, `y` and `orientation`) and the grid structure (`x`, `y` and the `scents`
array). The function implementation should be able to modify the robot
position and orientation and, if required, the `scents` list. Also the
*registration* of the command char (associated with the new function) should
be added at the end of the file (`module.exports` sentence).

2. Add new command char in lexer (`robot.json`): Of course, the lexer should
be able to recognize the new command char. The char `B` (i.e.) should be
added to the list of chars for the `INSTRING` symbol (line 8 of `robot.jison`).

### Add new orientation
In example, we could divide the orientation by 45 degrees to have NW, NE,
SW and SE orientations in addition of N, S, W, and E. A char should be
choosen carefully to avoid collision with existing orientations and commands.

1. Add new orientations in ROTATION_TABLE in `commands.js`: following with the
example, four new keys should be added to the commands `R` and `L`, defining
the resulting orientation of a 45 degrees turn in each direction.

3. Also new orientations should be added in the TRANSLATION_TABLE in `commands.js`: four new entries in total should be added, determining the efect of the
forward command in every new orientation (a movement in diagonal, i.e.).

### Caveats
You should use a different char for every command and orientation, since the
language processor (currently) does not include context information. This
caveat could be solved by using context tags.

The extensibility should also be improved by allowing any command char in
the lexer and checking the command later, in the scanner. This approach would
require also a new translation function.


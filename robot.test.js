const parser = require('./robot')

let consoleOutput = []
const mockedConsole = output => consoleOutput.push(output)
beforeEach(() => {
	consoleOutput = []
	console.log = mockedConsole
})

test('reject invalid file format: no EOL', () => {
	expect(() => parser.parse('1 1')).toThrow(/Expecting \'EOL\'/)
})
test('reject invalid file format: expecting COORDINATE', () => {
	expect(() => parser.parse('1 1\n')).toThrow(/Expecting \'COORDINATE\'/)
})
test('reject invalid file format: no instruction string', () => {
	expect(() => parser.parse('5 3\n1 1 E\n')).toThrow(/Expecting \'INSTRING\'/)
})
test('reject invalid file format: no upper-right coordinate', () => {
	expect(() => parser.parse('1 1 E\nFLLFRLLFFF\n')).toThrow(/Expecting \'EOL\'/)
})
test('reject invalid instruction string: bad command J', () => {
	expect(() => parser.parse('1 10\n1 1 E\nJRL\n')).toThrow(/Expecting \'INSTRING\'/)
})
test('reject invalid orientation Y', () => {
	expect(() => parser.parse('1 1\n0 0 Y\n')).toThrow(/Expecting \'ORIENTATION\'/)
})
test('reject invalid upper-right coordinate', () => {
	expect(() => parser.parse('51 51\n')).toThrow(/Max coordinate value is 50/)
})
test('reject instruction string too long', () => {
	expect(() => parser.parse('5 5\n1 1 S\nRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR\n'))
		.toThrow(/Instruction string must be less than 100 chars/)
})
test('reject invalid negative upper-right coordinate', () => {
	expect(() => parser.parse('-10 10\n')).toThrow(/Expecting \'COORDINATE\'/)
})
test('reject coordinates outside grid', () => {
	expect(() => parser.parse('0 0\n1 1 E\nRLRLRL\n'))
		.toThrow(/Coordinates outside grid/)
})
test('accept one only robot going from 0,0 to 5,3', () => {
	expect(parser.parse('5 3\n0 0 N\nFRFLFRFLFRFFFL\n')).toBe(true)
	expect(consoleOutput).toEqual(['5 3 N '])
})
test('accept sample output given in the assignment', () => {
	consoleOuput = []
	expect(parser.parse(
		'5 3\n1 1 E\nRFRFRFRF\n3 2 N\nFRRFLLFFRRFLL\n0 3 W\nLLFFFLFLFL\n'))
		.toBe(true)
	expect(consoleOutput).toEqual(["1 1 E ", "3 3 N LOST", "2 3 S "])
})

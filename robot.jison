%{
const commands = require('./commands')
const grid = { x: 0, y: 0, scents: [] }
%}
%lex
// Add new instruction string items (commands) or orientations here
%%
[LRF]+                return 'INSTRING'
[NSEW]                return 'ORIENTATION'
//

' '+                  return 'SPACE'
[0-9]+                return 'COORDINATE'
<<EOF>>               return 'EOF'
\n+                   return 'EOL'
.                     return 'INVALID'
/lex

%start file
%ebnf
%% /* language grammar */
file
    : grid instructions+ EOF
    ;

grid
    : COORDINATE SPACE COORDINATE EOL
			{ grid.x = parseInt($1)
				grid.y = parseInt($3)
				// Negative values not allowed by the scanner
				if(grid.x > 50 || grid.y > 50)
					throw new Error('Max coordinate value is 50 !')
			}
    ;

instructions
    : COORDINATE SPACE COORDINATE SPACE ORIENTATION EOL INSTRING EOL
			{ const x = parseInt($1)
				const y = parseInt($3)
				const orientation = $5
				const robot = { x, y, orientation }
				const route = $7
				if(x > grid.x || y > grid.y)
					throw new Error('Coordinates outside grid !')
      	if(route.length >= 100)
					throw new Error('Instruction string must be less than 100 chars !')
				// No runtime error for invalid command, scanner will throw it before
				for(let cmd of route)
					commands[cmd](cmd, robot, grid)
				console.log(
					[robot.x, robot.y, robot.orientation, robot.lost].join(' ')
				)
			}
    ;
